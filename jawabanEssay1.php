<?php
/*
JAWABAN ESSAY C
create table customers (
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key (id));

create table orders (
    -> id int auto_increment,
    -> amount varchar(255),
    -> customer_id int,
    -> primary key (id),
    -> foreign key (customer_id) references customers(id));

JAWABAN ESSAY D
insert into customers (name, email, password)
    -> values
    -> ("John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");

insert into orders (amount, customer_id)
    -> values
    -> (500, 1),
    -> (200, 2),
    -> (750, 2),
    -> (250, 1),
    -> (400, 2);

JAWABAN ESSAY E
select customers.name as "customers_name", sum(amount) as "total_amount" from customers join orders on customers.id = orders.customer_id group by customers.name;
*/


//JAWABAN ESSAY A
function hitung($string_data)
{
    $temp_op = preg_replace('([^\\+\\-*\\:%\\^])', ' ', trim($string_data));
    $temp_op = explode(' ', trim($temp_op));

    foreach ($temp_op as $key => $val) {
        if ($val)
        $operators[] = $val;
    }

    $numbers = preg_replace('([^0-9])', ' ', trim($string_data));
    $numbers = explode(' ', $numbers);

    $i = 0;

    foreach ($numbers as $key => $val) {
        if ($key == 0) {
            $answer = $val;
            continue;
        }

        if ($val) {
            switch ($operators[$i]) {
                case '+':
                    $answer += $val;
                    break;

                case '-':
                    $answer -= $val;
                    break;

                case '*':
                    $answer *= $val;
                    break;

                case ':':
                    $answer /= $val;
                    break;

                case '^':
                    $answer ^= $val;
                    break;

                case '%':
                    $answer %= $val;
            }

            $i++;
        }
    }

    return $answer;
}
echo hitung("102*2") . "<br>";
echo hitung("2+3") . "<br>";
echo hitung("100:25") . "<br>";
echo hitung("10%2") . "<br>";
echo hitung("99-2") . "<br>";


?>